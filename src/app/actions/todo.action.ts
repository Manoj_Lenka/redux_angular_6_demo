import { Action } from '@ngrx/store';
import { ITodo } from '../modules/shared/models';

export namespace TodoAction {
    export const actionTypes = {
        ADD_TODO: '[Todo App] Add todo',
        TOGGLE_TODO: '[Todo App] Toggle todo',
        SHOW_TODO: '[Todo App] Show todos'
    };

    export class AddTodo implements Action {
        readonly type = actionTypes.ADD_TODO;

        constructor(public payload: string) { }
    }

    export class ToggleTodo implements Action {
        readonly type = actionTypes.TOGGLE_TODO;

        constructor(public payload: number) { }
    }

    export class ShowTodoList implements Action {
        readonly type = actionTypes.SHOW_TODO;

        constructor(public payload: ITodo) { }
    }

    export type Actions
        = AddTodo
        | ToggleTodo
        | ShowTodoList;
}
