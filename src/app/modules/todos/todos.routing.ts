import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TodoListComponent } from './components';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                component: TodoListComponent
            },
            {
                path: '**',
                redirectTo: '',
                pathMatch: 'full'
            }
        ])
    ],
    exports: [
        RouterModule
    ]
})

export class TodosRouting { }
