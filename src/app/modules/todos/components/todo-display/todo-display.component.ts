import { Component, OnInit, Input } from '@angular/core';

import { ITodo } from '../../../shared/models';

@Component({
  selector: 'app-todo-display',
  templateUrl: './todo-display.component.html',
  styleUrls: ['./todo-display.component.css']
})
export class TodoDisplayComponent implements OnInit {

  @Input() todoList: ITodo[] = [];

  constructor() { }

  ngOnInit() {
  }

}
