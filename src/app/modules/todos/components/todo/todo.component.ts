import { Component, OnInit, Input } from '@angular/core';
import { ITodo } from '../../../shared/models';
// import { SharedService } from '../../../shared/services';
import { Store } from '@ngrx/store';
import { State } from '../../../../app.module';
import { TodoAction } from '../../../../actions';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})
export class TodoComponent implements OnInit {

  @Input() todo: ITodo;

  constructor(
    private _store: Store<State>,
    // private _sharedService: SharedService,
  ) { }

  ngOnInit() {
  }

  toggleSelected(index: number): void {
    // this._sharedService.toggoleSelected(index);
    this._store.dispatch(new TodoAction.ToggleTodo(index));
  }

}
