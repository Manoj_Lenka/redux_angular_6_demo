import { NgModule } from '@angular/core';
import { SharedService } from './services';

@NgModule({
    providers: [
        SharedService,
    ],
})
export class SharedModule { }
