import { ITodo } from './../models';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';

export class SharedService {
    private todoList: ITodo[] = [];

    getTodoList = new BehaviorSubject<ITodo[]>(this.todoList);

    addTodoList(text: string): void {
        const todo: ITodo = {
            text,
            isCompleted: false,
            index: this.todoList.length
        };
        this.todoList = [...this.todoList, todo];
        this.getTodoList.next(this.todoList);
    }

    toggoleSelected(index: number): void {
        const newTodoList: ITodo[] = this.todoList.map((todo: ITodo) => {
            if (todo.index === index) {
                return { ...todo, isCompleted: !todo.isCompleted };
            }
            return todo;
        });
        this.todoList = newTodoList;
        this.getTodoList.next(this.todoList);

    }

}
