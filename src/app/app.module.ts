import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { StoreModule, combineReducers, ActionReducerMap } from '@ngrx/store';

import {
  HeaderComponent,
  BodyComponent,
  RootComponent,
  HomeComponent,
} from './components';
import { AppRouting } from './app.routing';
import { SharedModule } from './modules/shared/shared.module';
import { TodoReducer } from './reducers';
import { TodoAction } from './actions';
import { ITodo } from './modules/shared/models';

export interface State {
  todos: ITodo[];
}

export const reducers: ActionReducerMap<State> = {
  todos: TodoReducer.reducer,
};

@NgModule({
  declarations: [
    HeaderComponent,
    RootComponent,
    BodyComponent,
    HomeComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule,
    CommonModule,
    AppRouting,
    SharedModule,
    StoreModule.forRoot(reducers),
    StoreDevtoolsModule.instrument({
      maxAge: 5,
    }),
  ],
  providers: [],
  bootstrap: [RootComponent]
})
export class AppModule { }
