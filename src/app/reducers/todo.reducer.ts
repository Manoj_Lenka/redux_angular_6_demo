import { ITodo } from '../modules/shared/models';
import { TodoAction } from './../actions';

export namespace TodoReducer {

    export function reducer(state: ITodo[] = [], action: TodoAction.Actions): ITodo[] {

        switch (action.type) {

            case TodoAction.actionTypes.ADD_TODO: {

                const newState: ITodo = {
                    index: state.length,
                    text: action.payload.toString(),
                    isCompleted: false
                };
                return [...state, newState];

            }

            case TodoAction.actionTypes.SHOW_TODO: {

                return state;

            }

            case TodoAction.actionTypes.TOGGLE_TODO: {

                const newState = state.map((todo: ITodo) => {
                    if (todo.index === +action.payload) {
                        return {...todo, isCompleted: !todo.isCompleted};
                    }
                    return todo;
                });
                return newState;
            }

            default: {
                return state;
            }

        }

    }

}
