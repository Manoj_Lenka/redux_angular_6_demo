import { State } from './../app.module';

export namespace TodoSelector {
    export const getTodoList = (state: State) => state.todos;

    export const getPendingTodosCount = (state: State) => state.todos.filter(todo => !todo.isCompleted).length;
}
