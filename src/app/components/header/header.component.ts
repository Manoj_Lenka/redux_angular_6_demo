import { Component, OnInit, Input } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { Store } from '@ngrx/store';
import { State } from '../../app.module';
import { TodoSelector } from '../../selectors/todo.selector';
// import { SharedService } from '../../modules/shared/services';
// import { ITodo } from '../../modules/shared/models';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  private subscriptions: Subscription[] = [];

  count = 0;

  // count$: Observable<number>;

  constructor(
    // private _sharedService: SharedService,
    private _store: Store<State>,
  ) { }

  ngOnInit() {
    this.getTodoCount();

    // "async" pipe to the rescue
    // this.count$ = this._store.select(TodoSelector.getPendingTodosCount);
  }

  getTodoCount(): void {
    this.subscriptions.push(
      // this._sharedService
      //   .getTodoList
      //   .subscribe((todoList: ITodo[]) => {
      //     this.count = todoList.length;
      //   })

      this._store
        .select(TodoSelector.getPendingTodosCount)
        .subscribe((count: number) => {
          this.count = count;
        })
    );
  }

}
