import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
// import { ITodo } from '../../modules/shared/models';
// import { SharedService } from '../../modules/shared/services';
import { Subscription } from 'rxjs/internal/Subscription';
import { State } from '../../app.module';
import { Store } from '@ngrx/store';
import { TodoSelector } from '../../selectors/todo.selector';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy {

  private subscriptions: Subscription[] = [];

  count = 0;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private _store: Store<State>,
    // private _sharedService: SharedService,
  ) { }

  ngOnInit() {
    this.getTodoCount();
  }

  getTodoCount(): void {
    this.subscriptions.push(
      // this._sharedService
      //   .getTodoList
      //   .subscribe((todoList: ITodo[]) => {
      //     this.count = todoList.length;
      //   })

      this._store
      .select(TodoSelector.getPendingTodosCount)
      .subscribe((count: number) => {
        this.count = count;
      })
    );
  }

  navigateToTodoList(): void {
    this._router.navigate(['./todos'], {
      relativeTo: this._activatedRoute
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach((s: Subscription) => s.unsubscribe());
  }

}
